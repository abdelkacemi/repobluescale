var appStorage = angular.module('appStorage', [ 'ngRoute' ]);
appStorage.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/login', {
		templateUrl : "login.html",
		controller : 'LoginCtrl'
	}).when('/', {
		templateUrl : "dump",
		controller : 'DumpCtrl'
	}).when('/dossier', {
		templateUrl : "dossier.html",
		controller : 'DossierCtrl'
	}).otherwise({
		redirectTo : '/home'
	});

} ]);
