appStorage.factory('GlobalFactory', function($http,$q) {
	var dumpItems = [];
	var dossierItems = [];

	for (var i = 0; i < 500; i++) {
		dumpItems.push({
			id : i,
			host : "Host " + i,
			port : "Port " + i,
			database : "Base de données" + i,
			user : "Utilisateur " + i,
			password : "Mot de passe " + i
		});
	}
	for (var i = 0; i < 50; i++) {
		dossierItems.push({
			id : i,
			host : "Host " + i,
			chemin : "Chemin " + i,
			user : "Utilisateur " + i,
			password : "Mot de passe " + i,
			periodicite : "Périodicité " + i
		});
	}
	
	
	var factory = {
		posts : false,
		getDumps : function() {
			/*var deferred=$q.defer();
			$http({method: 'GET',url: 'posts.json'}).then(function successCallback(response) {
				factory.posts=response.data;
				$timeout(function(){
					deferred.resolve(factory.posts);
				},1000);
			  }, function errorCallback(response) {
			    deferred.reject('Impossible de récupérer les articles');
			  });
		    return deferred.promise;*/
		},
		getDossiers : function() {
			/*var deferred=$q.defer();
			$http({method: 'GET',url: 'posts.json'}).then(function successCallback(response) {
				factory.posts=response.data;
				$timeout(function(){
					deferred.resolve(factory.posts);
				},1000);
			  }, function errorCallback(response) {
			    deferred.reject('Impossible de récupérer les articles');
			  });
		    return deferred.promise;*/
		},getUser : function() {
			$http.post({url: 'user',data:$rootScope.credentials}).then(function successCallback(response) {
				/*factory.posts=response.data;
				$timeout(function(){
					deferred.resolve(factory.posts);
				},1000);*/
			  }, function errorCallback(response) {
			    //deferred.reject('Impossible de récupérer les articles');
			  });
		    return deferred.promise;
		},getDumps : function(offset, limit) {
			return dumpItems.slice(offset, offset + limit);
		},
		getTotalDumps : function() {
			return dumpItems.length;
		},getDossiers : function(offset, limit) {
			return dossierItems.slice(offset, offset + limit);
		},
		getTotalDossiers : function() {
			return dossierItems.length;
		}
	}
	return factory;
});