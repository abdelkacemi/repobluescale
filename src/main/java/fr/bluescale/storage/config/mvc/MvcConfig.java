/**
 *
 * Module: MvcConfig.java  
 * Author: AKC 
 * Copyright (c) 2016, BlueScale.
 * All rights reserved.
 */
package fr.bluescale.storage.config.mvc;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("home");
		registry.addViewController("/dump").setViewName("dump");
		registry.addViewController("/header").setViewName("/includes/header");
		registry.addViewController("/login").setViewName("login");
	}

}
