package fr.bluescale.storage.model.dao;

import org.springframework.data.repository.Repository;

import fr.bluescale.storage.model.User;

public interface UserRepository extends Repository<User, Long> {

	/**
	 * Returns the {@link Customer} with the given identifier.
	 * 
	 * @param user
	 *            the object user to search for.
	 * @return
	 */
	User findUserByusername(String username);

}
