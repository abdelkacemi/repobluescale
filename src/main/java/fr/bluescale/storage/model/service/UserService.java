package fr.bluescale.storage.model.service;

import fr.bluescale.storage.model.User;

public interface UserService {
	/**
	 * Returns the {@link Customer} with the given identifier.
	 * 
	 * @param user
	 *            the object user to search for.
	 * @return
	 */
	User findUserByusername(String username);
}
